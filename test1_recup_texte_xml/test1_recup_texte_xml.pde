/*
Récupération des éléments textuels dans un fichier xml corrompu (fichier docx corrompu à la base)
Permet en autre de récupérer 3 mois de travail de traduction.

Copyleft pour tous les zozos et les loulous.
*/

// le document xml à exploiter (attention on commencera à chercher les child à partir de la balise <body> et non <document>)
String ba = "document2.xml";


void setup() {
  XML xml = loadXML(ba);

  // la variable String dans laquelle je vais ajouter les éléments textuels récupérés dans mon XML au fur et à mesure.
  String texte = "";

  // Je récupère d'abord les elts dans la balise body
  XML niveau_zero = xml.getChild("body");
  // évidemment il y a bcp de balises <p> dans cette balise <body> il convient de les récupérer toutes.
  XML[] niveau_un = niveau_zero.getChildren("p");

  // je scanne ma boucle avec toutes mes balises <p>
  // pour y récupérer mes balises <r>... et oui
  for (int k=0; k<niveau_un.length; k++) {
    XML niveau_deux = niveau_un[k].getChild("r");
    // si nous avons bien une balise <r> dans la balise <p> alors on peut
    // regarder s'il y a une balise <t>, ces fameuses balises qui contiennent 
    // des elements textuels.
    if (niveau_deux != null) {
      XML[] children = niveau_deux.getChildren("t");
      // j'ajoute le contenu textuel récolté à ma variable texte.
      for (int i=0; i<children.length; i++) {
        texte += children[i].getContent();
      }
    }
  }

  // petite manipe peu élégante pour imprimer le contenu de la variable texte
  // dans un fichier .txt
  if (texte != null) {
    String textFinal [] = {
      "  ", " "
    };
    textFinal = append(textFinal, texte);
    saveStrings("testExport.txt", textFinal); 
    println(texte);
  }
}

void draw() {
}

